﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LaserTrap : MonoBehaviour
{
    [SerializeField]
    LineRenderer lineRenderer;
    [SerializeField]
    int damage = 10;
    [SerializeField]
    float maxAnimationDuration = 1.0f;
    [SerializeField]
    float minHeight = -0.8f;
    [SerializeField]
    float maxHeight = 0.8f;
    [SerializeField]
    BoxCollider myCollider;

    void OnValidate()
    {
        if (!lineRenderer)
        {
            lineRenderer = GetComponentInChildren<LineRenderer>();
        }
        if (!myCollider)
        {
            myCollider = GetComponent<BoxCollider>();
        }
    }

    void OnEnable()
    {
        StartCoroutine(AnimateLaser());
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            Trigger(other);
        }
    }

    void Trigger(Collider other)
    {
        HealthManager healthManager = other.GetComponent<HealthManager>();
        if (!healthManager) { return; }

        healthManager.Damage(damage);
    }

    IEnumerator AnimateLaser()
    {
        Vector3 laserStartingPoint = lineRenderer.GetPosition(0);
        Vector3 laserEndingPoint = lineRenderer.GetPosition(1);
        Vector3 colliderCenter = myCollider.center;

        float currentY = minHeight;
        float elapsedTime = 0;

        while (gameObject.activeSelf)
        {
            if (Mathf.Abs(currentY) >= Mathf.Abs(maxHeight))
            {
                float temp = minHeight;
                minHeight = maxHeight;
                maxHeight = temp;
                //elapsedTime = 0;
                elapsedTime = Time.deltaTime;
            }
            currentY = Mathf.Lerp(minHeight, maxHeight, elapsedTime / maxAnimationDuration);
            elapsedTime += Time.deltaTime;

            laserStartingPoint.y = currentY;
            laserEndingPoint.y = currentY;
            lineRenderer.SetPosition(0, laserStartingPoint);
            lineRenderer.SetPosition(1, laserEndingPoint);

            colliderCenter.y = currentY;
            myCollider.center = colliderCenter;
            yield return new WaitForEndOfFrame();

        }
    }
}
