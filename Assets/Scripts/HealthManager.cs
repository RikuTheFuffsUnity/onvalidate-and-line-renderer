﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityStandardAssets.Characters.FirstPerson;

/// <summary>
/// Manages the health of the player
/// </summary>
public class HealthManager : MonoBehaviour
{
    public int maxHealth;
    public int health;
    public Text lblHealth;
    public Slider sliderHealth;
    public GameObject pnlDeath;
    public Button btnRespawn;
    Vector3 safeRespawnPoint;

    bool isDead { get { return currentHealthPercentage < 0.01f; } }
    float currentHealthPercentage { get { return ((float)(health) / (float)(maxHealth)); } }

    void Start()
    {
        health = maxHealth;
        sliderHealth.value = currentHealthPercentage;
        UpdateHealthbarColor();
        pnlDeath.SetActive(false);
        btnRespawn.onClick.AddListener(Respawn);
        safeRespawnPoint = transform.position;
    }

    public void Heal(int amount)
    {
        Damage(-amount);
    }

    public void Damage(int damageTaken)
    {
        if (isDead) { return; }
        health -= damageTaken;
        if (health < 1)
        {
            Die();
        }
        if (health > maxHealth)
        {
            health = maxHealth;
        }
        sliderHealth.value = currentHealthPercentage;
        Debug.LogError(currentHealthPercentage);
        UpdateHealthbarColor();
    }

    void UpdateHealthbarColor()
    {
        Image fillImage = sliderHealth.fillRect.GetComponent<Image>();
        if (currentHealthPercentage >= 0.75f)
        {
            fillImage.color = Color.green;
        }
        else if (currentHealthPercentage >= 0.25f && currentHealthPercentage < 0.75f)
        {
            fillImage.color = Color.yellow;
        }
        else
        {
            fillImage.color = Color.red;
        }
    }

    public void Die()
    {
        RigidbodyFirstPersonController movementController = GetComponent<RigidbodyFirstPersonController>();
        movementController.enabled = false;
        movementController.mouseLook.SetCursorLock(false);
        pnlDeath.SetActive(true);
    }

    public void Respawn()
    {
        transform.position = safeRespawnPoint;
        pnlDeath.SetActive(false);
        RigidbodyFirstPersonController movementController = GetComponent<RigidbodyFirstPersonController>();
        movementController.mouseLook.SetCursorLock(true);
        movementController.enabled = true;
        health = maxHealth;
        sliderHealth.value = currentHealthPercentage;
        UpdateHealthbarColor();
    }
}
